import Vue from 'vue';
import Vuex from 'vuex';
import api from '../api';
import helpers from '../helpers';
Vue.use(Vuex);

export const store = new Vuex.Store({
    strict: false,
    state: {
        mazeId: false,
        size: null,
        cells: null,
        domokun: null,
        pony: null,
        finish: null,
        solution: null,
        loader: false,
        gameOver: false,
        maze: false,
        currentLocation: null
    },
    getters:  {
         cells : state => state.cells,
         cell: state => (i, j) => state.cells[i][j],
         width : state => state.size[0],
         height : state => state.size[1],
         pony : state => state.pony,
         domokun : state => state.domokun,
         finish : state => state.finish,
         loader : state => state.loader,
         gameOver : state => state.gameOver,
         solution : state => state.solution
    },
    actions: {
        initialise: ({commit, state, dispatch}) => {
            api.fetchMazeId({state: state}, data => {
                commit('MAZE_ID', data['maze_id']);

                api.fetchMaze({state: state}, data => {
                    commit('LOADER', true);
                    commit('INIT_METADATA', data);
                    commit('INIT_CELLS', data);

                    // Solve maze using pony as a starting point
                    dispatch('solve', {
                       i: Math.floor(state.pony / state.size[0]),
                       j: state.pony % state.size[0]
                    });
                    commit('RENDER_SOLUTION', helpers.solution);
                    commit('LOADER', false);
                });
            }, error => {
                console.log(error);
            });
        },
        solve: ({commit, state, dispatch}, {i, j}) => {
            helpers.solve(i, j, state, commit);
        },
        newMaze: ({commit, state, dispatch}) => {
            commit('LOADER', true);
            commit('GAME_OVER', false);
            dispatch('initialise');
        },
        walk: ({commit, state, dispatch}) => {
            state.currentLocation = state.solution.pop();
            dispatch('walkNext');
        },
        walkNext: ({commit, state, dispatch}) => {
            var next = state.solution.pop();
            if (next) {
                var direction = helpers.getWalkDirection(state.currentLocation, next);
                api.walkAStep({state: state}, direction, data => {
                    if (data['state-result'] == "Move accepted") {
                        commit('WALKED', {
                            current: state.currentLocation,
                            next: next
                        });
                        state.currentLocation = next;
                        dispatch('walkNext');
                    } else {
                        commit('GAME_OVER', 'https://ponychallenge.trustpilot.com' + data['hidden-url']);
                        console.log(data);
                    }
                });
            }
        }
    },
    mutations: {
        INIT_METADATA (state, data) {
            state.pony = data.pony;
            state.domokun = data.domokun;
            state.finish = data['end-point'];
            state.size = data.size;
            state.solution = null;
            helpers.solution = [];
        },

        // Initialise cells using i x j matrix
        INIT_CELLS (state, {data}) {
            var cells = [];
            var row = [];
            data.forEach(function(cell, index) {
                if (index > 0 && index % state.size[0] == 0) {
                    cells.push(row);
                    row = [];
                }
                row.push({
                    id: index,
                    n: cell.indexOf('north') != -1,
                    e: (index + 1) % state.size[0] == 0 || data[index + 1].indexOf('west') != -1,
                    s: (index >= state.size[0] * state.size[1] - state.size[0]) || data[index + state.size[0]].indexOf('north') != -1,
                    w: cell.indexOf('west') != -1,
                    pony: index == state.pony,
                    domokun: index == state.domokun,
                    finish: index == state.finish,
                    solution: false
                });
            });
            cells.push(row);
            state.cells = cells;
        },
        RENDER_SOLUTION (state, data) {
            state.solution = data;
            var prev = false;
            for (var i = 0; i < data.length; i++) {
                var solutionClass = ['solution'];
                var next = data[i + 1];
                var prev = data[i - 1];
                solutionClass = helpers.getSolutionDirections(prev, data[i], solutionClass);
                solutionClass = helpers.getSolutionDirections(next, data[i], solutionClass);
                state.cells[data[i].coords[0]][data[i].coords[1]].solution = solutionClass.length > 0 ? solutionClass.join(' ') : false;
            }
        },
        LOADER (state, toggle) {
            state.loader = toggle;
        },
        MAZE_ID (state, id) {
            state.maze = id;
        },
        GAME_OVER (state, status) {
            state.gameOver = status;
        },
        WALKED (state, {current, next}) {
            state.cells[current.coords[0]][current.coords[1]].pony = false;
            state.cells[next.coords[0]][next.coords[1]].pony = true;
        }
    }
});