export default {
    solution: [],
    solve: function (i, j, state) {
        var cell = state.cells[i][j];

        if (cell.finish) {
            cell.coords = [i, j];
            this.solution.push(cell);
            return true;
        }
        if (cell.walked) {
            return false;
        }
        cell.walked = true;

        if (
            (!cell.s && i < state.size[1] - 1 && this.solve(i + 1, j, state)) ||
            (!cell.w && j > 0                 && this.solve(i, j - 1, state)) ||
            (!cell.e && j < state.size[0] - 1 && this.solve(i, j + 1, state)) ||
            (!cell.n && i > 0                 && this.solve(i - 1, j, state))
        ) {
            cell.coords = [i, j];
            this.solution.push(cell);
            return true;
        }
        return false;
    },
    getSolutionDirections: function (adjacent, cell, classes) {
        if (adjacent && adjacent.id > cell.id) {
            classes.push(adjacent.id - cell.id == 1 ? 'right' : 'down');
        }
        if (adjacent && adjacent.id < cell.id) {
            classes.push(adjacent.id - cell.id == -1 ? 'left' : 'up');
        }
        return classes;
    },
    getWalkDirection: function (current, next) {
        if (next.id > current.id) {
            return next.id - current.id == 1 ? 'east' : 'south';
        }
        if (next.id < current.id) {
            return next.id - current.id == -1 ? 'west' : 'north';
        }
        return false;
    }
}