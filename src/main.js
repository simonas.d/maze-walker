import 'babel-polyfill';
import Vue from 'vue';
import App from './components/App.vue';
import {store} from './store';

var app = new Vue({
    render: h => h(App),
    mounted: function() {
        store.dispatch('initialise');
    },
    store
})
window.onload = function () {
    app.$mount('#wrap')
}