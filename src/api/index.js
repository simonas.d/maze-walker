import axios from 'axios';
var qs = require('qs');
var url = 'https://ponychallenge.trustpilot.com/pony-challenge/maze';

// Promised based client library for API requests
export default {
    fetchMazeId: function ({state}, cb, cbError) {

        axios.post(url,
            {
                "maze-width": 15,
                "maze-height": 25,
                "maze-player-name": "Apple Bloom"
            }
         ).then((response) => {
            cb(response.data);
        });
    },
    fetchMaze: function ({state}, cb, cbError) {
        axios.get(url + '/' +  state.maze,
         ).then((response) => {
            cb(response.data);
        });
        this.fetchMazePrint({state: state});
    },
    fetchMazePrint: function ({state}, cb, cbError) {
        axios.get(url + '/' +  state.maze + '/print',
         ).then((response) => {
            console.log(response.data);
        });
    },

    walkAStep: function ({state}, direction, cb, cbError) {
        axios.post(url + '/' +  state.maze,
            {
                "direction": direction
            }
         ).then((response) => {
            cb(response.data);
        });
    },
}