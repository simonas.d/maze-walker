'use strict';

var webpack = require('webpack'),
    jsPath  = '',
    path = require('path'),
    srcPath = path.join(__dirname, '/src');

var config = {
    target: 'web',
    entry: {
        app: path.join(srcPath, 'main.js')
    },
    output: {
        path: path.resolve(__dirname, jsPath, 'build'),
        filename: 'build.js'
    },
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.scss$/,
                include: /styles/,
                loader: 'style!css!sass'
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false },
            output: { comments: false }
        }),
        new webpack.NoEmitOnErrorsPlugin()
    ]
};

module.exports = config;